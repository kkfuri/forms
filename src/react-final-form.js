import React from 'react'
import IMask from 'imask'
import PropTypes from 'prop-types'
import { Form, useField } from 'react-final-form'
import { object, string, date, bool } from 'yup'

function validationSchema() {
  return object().shape({
    firstName: string().min(3).required(),
    lastName: string().max(8).required(),
    birthdate: date().required(),
    email: string().email().required(),
    gender: bool().required(),
    remember: bool().required(),
  });
}

const formArray = [
  { id: "firstName", type: "text" },
  { id: "lastName", type: "text" },
  { id: "birthdate", type: "date" },
  { id: "email", type: "text" },
  {
    id: "gender",
    type: "select",
    options: [
      { value: 0, label: "Masculino" },
      { value: 1, label: "Feminino" },
    ],
  },
  { id: "remember", type: "checkbox" },
];

async function validate(values) {
  const validator = validationSchema()

  try {
    await validator.validate(values, { abortEarly: false })
  } catch (err) {
    const errors = err.inner.reduce((formError, innerError) => ({
      ...formError,
      [innerError.path]: innerError.message
    }), {})

    return errors
  }
}

validate.propTypes = {
  value: PropTypes.string.isRequired
}

function RenderInput({ children, type, options, id, ...rest }) {
  if (type === "select") {
    return (
      <select name={id} {...rest}>
        {Array.isArray(options) &&
          options.map((opt, i) => {
            return (
              <option value={opt.value} key={`option-${i}`}>
                {opt.label}
              </option>
            );
          })}
      </select>
    );
  }

  return (
    <input {...rest} type={type} id={id}>
      {children}
    </input>
  );
}

InputForm.propTypes = {
  RenderInput: PropTypes.node,
  id: PropTypes.string.isRequired,
  type: PropTypes.string,
  options: PropTypes.array
}

function InputForm({ form, id , formatter, type, options}) {
  const { meta, input } = useField(id, form);

  console.log(`Rendering field: ${id}`)

  return (
    <>
      <label style={{ marginTop: 16}} >{id}</label>

      <RenderInput
        {...input}
        options={options}
        value={formatter ? formatter(input.value) : input.value}
        placeholder={id}
        type={type}
      />

      {meta.touched && meta.error && <span>{meta.error}</span>}
    </>
  );
}

InputForm.propTypes = {
  form: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  formatter: PropTypes.func,
  type: PropTypes.string,
  options: PropTypes.array
}

function RenderFinalForm() {
  function onSubmit(values) {
    return alert(JSON.stringify(values, null, 2));
  }

  return (
    <Form
      onSubmit={onSubmit}
      // validate={validate} // Ao remover a validação o controle de rerenderização vouncionou
      subscription={{ submitting: true, pristine: true }}
      render={({ handleSubmit, form, submitting, pristine }) => (
        <form onSubmit={handleSubmit}>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              width: "48%",
            }}
          >
            {formArray.map((field, i) => {
              return (
                <InputForm
                  {...field}
                  form={form}
                  key={`input-final-test-${i}`}
                />
              );
            })}
          </div>
          <div style={{ display: "flex", marginTop: 32 }}>
            <button
              onClick={() => form.reset()}
              disabled={submitting || pristine}
              type="reset"
            >
              Cancel
            </button>
            <button type="submit">Submit</button>
          </div>
        </form>
      )}
    />
  );
}

function FinalForm() {

  return <RenderFinalForm />;
}

export default FinalForm