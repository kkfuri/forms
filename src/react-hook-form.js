import React, { forwardRef } from "react";
import { useForm } from "react-hook-form";
import { DevTool } from "react-hook-form-devtools";
import { isValidCpf } from "@brazilian-utils/is-valid-cpf";
import isValidCep from "@brazilian-utils/is-valid-cep";

const Input = forwardRef(({ name, label, error, ...props }, ref) => {
  console.log("Render: ", name);
  return (
    <div style={{ display: "flex", margin: "8px 0" }}>
      <label htmlFor={name} style={{ minWidth: 200, textAlign: 'right', marginRight: 16 }}>
        {label}
      </label>
      <input name={name} id={name} ref={ref} {...props} />
      {error && <span style={{ marginLeft: 16, color: "red" }}>{error}</span>}
    </div>
  );
});

function ReactHookForm() {
  const { register, handleSubmit, errors, formState, control } = useForm({
    mode: "onChange",
  });

  const { isValid } = formState;

  function onSubmit(values) {
    console.log(values);
  }

  return (
    <div>
      <form
        style={{ maxWidth: 700 }}
        onSubmit={handleSubmit(onSubmit)}
      >
        <Input
          name="email"
          label="E-mail"
          type="email"
          ref={register({
            required: "O e-mail é obrigatório",
            validate: (value) =>
              value.includes("@") || "Digite um e-mail válido",
          })}
          error={errors.email?.message}
        />
        <Input
          name="senha"
          label="Password"
          type="password"
          ref={register({
            required: "A senha é obrigatória",
            minLength: { value: 6, message: "A senha deve ter 6 digitos" },
          })}
          error={errors.senha?.message}
        />
        <Input
          name="nome"
          label="Nome"
          type="text"
          ref={register()}
          error={errors.nome?.message}
        />
        <Input
          name="sobrenome"
          label="Sobrenome"
          type="text"
          ref={register()}
          error={errors.sobrenome?.message}
        />
        <Input
          name="cpf"
          label="CPF"
          type="text"
          ref={register({
            required: "O CPF é obrigatório",
            validate: (value) => isValidCpf(value) || "Insira um CPF válido",
          })}
          error={errors.cpf?.message}
        />
        <Input
          name="cep"
          label="CEP"
          type="text"
          ref={register({
            required: "O CEP é obrigatório",
            validate: (value) => isValidCep(value) || "Digite um CEP válido",
          })}
          error={errors.cep?.message}
        />
        <Input
          name="rua"
          label="Rua"
          type="text"
          ref={register({
            required: "A rua é obrigatória",
          })}
          error={errors.rua?.message}
        />
        <Input
          name="bairro"
          label="Bairro"
          type="text"
          ref={register({
            required: "O bairro é obrigatório",
          })}
          error={errors.bairro?.message}
        />
        <Input
          name="cidade"
          label="Cidade"
          type="text"
          ref={register({
            required: "A cidade é obrigatória",
          })}
          error={errors.cidade?.message}
        />
        <Input
          name="estado"
          label="Estado"
          type="text"
          ref={register({
            required: "O estado é obrigatório",
            minLength: {
              value: 2,
              message: "o estado deve ter pelo menos 2 dígitos",
            },
          })}
          error={errors.estado?.message}
        />
        <Input
          label="Eu aceito os termos"
          id="terms"
          type="checkbox"
          name="terms"
          ref={register({
            required: "Os termos são obrigatórios para usar a plataforma",
          })}
        />
        <button type="submit" disabled={!isValid} style={{ marginLeft: 220 }}>
          Enviar
        </button>
      </form>

      <DevTool control={control} />
    </div>
  );
}

export default ReactHookForm;
