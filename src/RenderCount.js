import React from 'react'

export default function RenderCount() {
  const renders = React.useRef(0)

  return (
    <span style={{
      marginLeft: 40,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      background: '#900',
      color: '#fff',
      width: 30,
      height: 30,
      borderRadius: '50%',
    }}>
      {++renders.current}
    </span>
  )
}