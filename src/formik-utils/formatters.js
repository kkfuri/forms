import IMask from 'imask';
import formatCpf from '@brazilian-utils/format-cpf';

export { formatCpf };

export const unformatNumber = (value = '') => String(value).replace(/\D/g, '');

export const formatCep = (value = '') => {
  const cepMask = IMask.createMask({ mask: '00000-000' });
  const unformated = unformatNumber(value);
  return cepMask.resolve(unformated);
};
