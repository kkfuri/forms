import { isValidCpf } from '@brazilian-utils/is-valid-cpf';
import isValidCep from '@brazilian-utils/is-valid-cep';

export { isValidCpf, isValidCep };