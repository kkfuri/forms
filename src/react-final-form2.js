import React from 'react'

import { Form, Field } from 'react-final-form'
import { object, string, date, bool } from 'yup'

import RenderCount from './RenderCount'

function validationSchema() {
  return object().shape({
    firstName: string().min(3).required(),
    lastName: string().max(8).required(),
    birthdate: date().required(),
    email: string().email().required(),
    gender: bool().required(),
    remember: bool().required(),
  });
}

async function validate(values) {
  const validator = validationSchema()

  try {
    await validator.validate(values, { abortEarly: false })
  } catch (err) {
    const errors = err.inner.reduce((formError, innerError) => ({
      ...formError,
      [innerError.path]: innerError.message
    }), {})

    return errors
  }
}

const Input = ({
  name,
  label,
  type = 'text',
  error,
  ...props
}) => {

  return (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      marginBottom: '16px'
    }}>
      <label htmlFor={name} style={{ marginBottom: '8px' }}>
        {label}
      </label>
      <input {...props} id={name} type={type} />
      {error
        && (
          <span style={{ marginTop: '8px', color: "red" }}>
            {error}
          </span>
        )
      }
    </div>
  );
};
// const required = value => (value ? undefined : 'Required')
function RenderFinalForm() {
  function onSubmit(values) {
    return alert(JSON.stringify(values, null, 2));
  }

  return (
    <Form
      onSubmit={onSubmit}
      subscription={{ submitting: true, pristine: true }}
      // validate={validate} // Ao remover a validação o controle de rerenderização vouncionou
      render={({
        handleSubmit,
        form,
        submitting,
        pristine
      }) => (
          <form
            onSubmit={handleSubmit}
            style={{
              display: "flex",
              flexDirection: "column",
              width: 280,
            }}
          >

            <Field name='first_name'>
              {({ input, meta }) => (
                <div style={{ display: 'flex', width: 280 }}>
                  <Input
                    label='Nome: '
                    error={meta.error}
                    {...input}
                  />
                  <RenderCount />
                </div>
              )}
            </Field>

            <Field name='last_name'>
              {({ input, meta }) => (
                <div style={{ display: 'flex', width: 280 }}>
                  <Input
                    label='Sobrenome: '
                    error={meta.error}
                    {...input}
                  />
                  <RenderCount />
                </div>
              )}
            </Field>

            <Field name='birth_date'>
              {({ input, meta }) => (
                <div style={{ display: 'flex', width: 280 }}>
                  <Input
                    label='Data nascimento: '
                    error={meta.error}
                    {...input}
                  />
                  <RenderCount />
                </div>
              )}
            </Field>

            <Field name='email'>
              {({ input, meta }) => (
                <div style={{ display: 'flex', width: 280 }}>
                  <Input
                    label='E-mail: '
                    error={meta.error}
                    {...input}
                  />
                  <RenderCount />
                </div>
              )}
            </Field>

            <Field name='remember' type='checkbox'>
              {({ input, meta }) => (
                <div style={{ display: 'flex', width: 280 }}>
                  <Input
                    label='Lembrar: '
                    type="checkbox"
                    error={meta.error}
                    {...input}
                  />
                  <RenderCount />
                </div>
              )}
            </Field>

            <div style={{ display: "flex", marginTop: 32 }}>
              <button
                onClick={() => form.reset()}
                disabled={submitting || pristine}
                type="reset"
              >
                Cancelar
              </button>

              <button
                disabled={submitting}
                type="submit"
              >
                Enviar
              </button>

            </div>
          </form>
        )}
    />
  );
}

export default RenderFinalForm