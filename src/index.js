import React from "react";
import ReactDOM from "react-dom";
import { Switch, Route, Link, BrowserRouter } from "react-router-dom";
import ReactHookForm from "./react-hook-form";
import FormikForm from "./formik";
import FinalForm from './react-final-form'
import FinalForm2 from './react-final-form2'

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <ul style={{ padding: 20, margin: 20, borderBottom: "1px solid #ccc" }}>
        <li>
          <Link to="/react-hook-form">React Hook Form</Link>
        </li>
        <li>
          <Link to="/formik">Formik</Link>
        </li>
        <li>
          <Link to="/react-final-form">React Final Form</Link>
        </li>
        <li>
          <Link to="/react-final-form2">React Final Form2</Link>
        </li>
      </ul>
      <Switch>
        <Route exact path="/react-hook-form">
          <ReactHookForm />
        </Route>
        <Route exact path="/formik">
          <FormikForm />
        </Route>
        <Route exact path="/react-final-form">
          <FinalForm />
        </Route>
        <Route exact path="/react-final-form2">
          <FinalForm2 />
        </Route>
      </Switch>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
