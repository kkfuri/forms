import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';

import { formatCep, formatCpf } from './formik-utils/formatters'

import { isValidCep, isValidCpf } from './formik-utils/validators'

const Input = ({ name, label, type = 'text', error, ...props }) => {
  console.log("Render: ", name);
  return (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      marginBottom: '16px'
    }}>
      <label htmlFor={name} style={{ marginBottom: '8px' }}>
        {label}
      </label>
      <input name={name} id={name} type={type} {...props} />
      {error
        && (
          <span style={{ marginTop: '8px', color: "red" }}>
            {error}
          </span>
        )
      }
    </div>
  );
};

const Checkbox = ({ name, label, error, ...props }) => {
  console.log("Render: ", name);
  return (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      marginBottom: '16px'
    }}>
      <div>
        <input type="checkbox" name={name} id={name} {...props} />
        <label htmlFor={name} style={{ marginBottom: '8px' }}>
          {label}
        </label>
      </div>
      {error
        && (
          <span style={{ marginTop: '8px', color: "red" }}>
            {error}
          </span>
        )
      }
    </div>
  );
};

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email('Email inválido')
    .required('Email é obrigatório'),
  password: Yup.string()
    .min(8, 'Mínimo de 8 caracteres')
    .required('Senha é obrigatória'),
  name: Yup.string()
    .required('O nome é obrigatório'),
  lastName: Yup.string()
    .required('O sobrenome é obrigatório'),
  cpf: Yup.string()
    .required('O CPF é obrigatório')
    .test({
      name: 'CPF',
      message: 'O CPF não é válido',
      test: value => isValidCpf(value),
    }),
  cep: Yup.string()
    .required('O CEP é obrigatório')
    .test({
      name: 'CEP',
      message: 'O CEP não é válido',
      test: value => isValidCep(value),
    }),
  street: Yup.string()
    .required('A rua é obrigatória'),
  neighbourhood: Yup.string()
    .required('O bairro é obrigatório'),
  city: Yup.string()
    .required('A cidade é obrigatória'),
  state: Yup.string()
    .required('O estado é obrigatório'),
  terms: Yup.bool()
    .required('Para continuar os termos devem ser aceitos'),
});

const FormikForm = () => {
  const initialValues = {
    email: '',
    password: '',
    name: '',
    lastName: '',
    cpf: '',
    cep: '',
    street: '',
    neighbourhood: '',
    city: '',
    state: '',
    terms: '',
  };

  const onSubmit = (values) => {
    console.log('Submit: ', values);
  }

  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
  });

  console.log('Touched: ', formik.touched);
  console.log('Errors: ', formik.errors);

  return (
    <div style={{ width: '50%' }}>
      <form onSubmit={formik.handleSubmit}>
        <Input
          name='email'
          label='Email'
          type='email'
          value={formik.values.email}
          onChange={formik.handleChange('email')}
          onBlur={formik.handleBlur('email')}
          error={formik.touched.email && formik.errors.email}
        />
        <Input
          name='password'
          label='Senha'
          value={formik.values.password}
          onChange={(e) => formik.setFieldValue('password', e.target.value)}
          onBlur={formik.handleBlur('password')}
          error={formik.touched.password && formik.errors.password}
        />
        <Input
          name='name'
          label='Nome'
          value={formik.values.name}
          onChange={formik.handleChange('name')}
          onBlur={formik.handleBlur('name')}
          error={formik.touched.name && formik.errors.name}
        />
        <Input
          name='lastName'
          label='Sobrenome'
          value={formik.values.lastName}
          onChange={formik.handleChange('lastName')}
          onBlur={formik.handleBlur('lastName')}
          error={formik.touched.lastName && formik.errors.lastName}
        />
        <Input
          name='cpf'
          label='CPF'
          value={formatCpf(formik.values.cpf)}
          onChange={formik.handleChange('cpf')}
          onBlur={formik.handleBlur('cpf')}
          error={formik.touched.cpf && formik.errors.cpf}
        />
        <Input
          name='cep'
          label='CEP'
          value={formatCep(formik.values.cep)}
          onChange={formik.handleChange('cep')}
          onBlur={formik.handleBlur('cep')}
          error={formik.touched.cep && formik.errors.cep}
        />
        <Input
          name='street'
          label='Rua'
          value={formik.values.street}
          onChange={formik.handleChange('street')}
          onBlur={formik.handleBlur('street')}
          error={formik.touched.street && formik.errors.street}
        />
        <Input
          name='neighbourhood'
          label='Bairro'
          value={formik.values.neighbourhood}
          onChange={formik.handleChange('neighbourhood')}
          onBlur={formik.handleBlur('neighbourhood')}
          error={formik.touched.neighbourhood && formik.errors.neighbourhood}
        />
        <Input
          name='city'
          label='Cidade'
          value={formik.values.city}
          onChange={formik.handleChange('city')}
          onBlur={formik.handleBlur('city')}
          error={formik.touched.city && formik.errors.city}
        />
        <Input
          name='state'
          label='Estado'
          value={formik.values.state}
          onChange={formik.handleChange('state')}
          onBlur={formik.handleBlur('state')}
          error={formik.touched.state && formik.errors.state}
        />
        <Checkbox
          name='terms'
          label='Eu aceito os Termos de Uso'
          value={formik.values.terms}
          onChange={formik.handleChange('terms')}
          onBlur={formik.handleBlur('terms')}
          error={formik.touched.terms && formik.errors.terms}
        />
        <button type='submit' disabled={!formik.isValid}>
          Enviar
      </button>
      </form>
    </div>
  );
};

export default FormikForm;